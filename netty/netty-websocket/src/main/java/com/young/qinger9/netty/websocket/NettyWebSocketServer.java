package com.young.qinger9.netty.websocket;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-30 12:09
 * @Description: Netty WebSocket 服务端
 */
public class NettyWebSocketServer {

    private static final Logger LOGGER = LogManager.getLogger(NettyWebSocketServer.class);
    private static final int DEFAULT_PORT = 8998;

    private int port;

    public NettyWebSocketServer(int port) {
        this.port = port;
    }

    public void start() throws Exception {
        LOGGER.info("netty websocket server is starting.");

        NioEventLoopGroup bossEventLoopGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerEventLoopGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossEventLoopGroup, workerEventLoopGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline()
                                    // websocket底层基于HTTP协议,先用HTTP协议处理数据,讲数据转换成HttpRequest请求对象.
                                    .addLast(new HttpServerCodec())
                                    // 数据分chunk快写出去（大数据量）
                                    .addLast(new ChunkedWriteHandler())
                                    // 请求数据聚合处理
                                    .addLast(new HttpObjectAggregator(1024 * 32))
                                    // 基于websocket协议处理，封装数据
                                    .addLast(new WebSocketServerProtocolHandler("/websocket"))
                                    // 响应数据websocket协议封装 -> http协议处理 -> browser client
                                    .addLast("netty-websocket-server-handler", new NettyWebSocketServerHandler());
                        }
                    });
            ChannelFuture channelFuture = serverBootstrap.bind(port).sync();
            LOGGER.info("netty websocket server is started, listened[" + port + "].");

            channelFuture.channel().closeFuture().sync();
        } finally {
            bossEventLoopGroup.shutdownGracefully();
            workerEventLoopGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception {
        NettyWebSocketServer nettyWebSocketServer = new NettyWebSocketServer(DEFAULT_PORT);
        nettyWebSocketServer.start();
    }
}
