package com.young.qinger9.netty.websocket;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-30 12:07
 * @Description: Netty WebSocket 服务端处理器
 */
public class NettyWebSocketServerHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    private static final Logger LOGGER = LogManager.getLogger(NettyWebSocketServerHandler.class);

    private static ChannelGroup websocketClients =
            new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) throws Exception {
        // websocket client发送的数据
        String request = msg.text();
        LOGGER.info("netty server receives request: " + request + ".");

        TextWebSocketFrame response = new TextWebSocketFrame("hello,this is websocket server.");
        ctx.writeAndFlush(response);
    }

    /**
     * websocket客户端跟服务端建立连接之后,触发.
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        websocketClients.add(ctx.channel());
    }

    /**
     * 断开连接,触发.
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        LOGGER.info("websocket client is closed,channel id: " +
                ctx.channel().id().asLongText() + "[" +
                ctx.channel().id().asShortText() + "]");
    }
}
