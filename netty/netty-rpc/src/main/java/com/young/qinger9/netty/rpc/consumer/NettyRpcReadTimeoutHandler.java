package com.young.qinger9.netty.rpc.consumer;

import com.young.qinger9.netty.rpc.common.RpcResponse;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 14:34
 * @Description: Netty客户端超时处理器
 */
public class NettyRpcReadTimeoutHandler extends ChannelInboundHandlerAdapter {

    private static final Logger LOGGER = LogManager.getLogger(NettyRpcReadTimeoutHandler.class);

    /**
     * 超时时间
     */
    private long timeout;

    public NettyRpcReadTimeoutHandler(long timeout) {
        this.timeout = timeout;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        RpcResponse rpcResponse = (RpcResponse) msg;

        long requestTime = NettyRpcReadTimeoutHolder.get(rpcResponse.getRequestId());
        long now = new Date().getTime();

        if (now - requestTime >= timeout) {
            rpcResponse.setTimeout(true);
            LOGGER.error("netty rpc response is marked as timeout status: " + rpcResponse);
        }
        NettyRpcReadTimeoutHolder.remove(rpcResponse.getRequestId());

        ctx.fireChannelRead(rpcResponse);
    }
}
