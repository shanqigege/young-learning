package com.young.qinger9.netty.rpc.consumer;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 11:37
 * @Description: 引用配置
 */
public class ReferenceConfig {
    /**
     * 默认超时时间
     */
    private static final long DEFAULT_TIMEOUT = 5000;
    /**
     * 默认服务主机
     */
    private static final String DEFAULT_SERVICE_HOST = "127.0.0.1";
    /**
     * 默认服务端口
     */
    private static final int DEFAULT_SERVICE_PORT = 8998;

    /**
     * 服务接口类
     */
    private Class serviceInterfaceClass;
    /**
     * 服务主机
     */
    private String serviceHost;
    /**
     * 服务端口
     */
    private int servicePort;
    /**
     * 超时时间
     */
    private long timeout;

    public ReferenceConfig(Class serviceInterfaceClass) {
        this(serviceInterfaceClass, DEFAULT_SERVICE_HOST, DEFAULT_SERVICE_PORT, DEFAULT_TIMEOUT);
    }

    public ReferenceConfig(Class serviceInterfaceClass, String serviceHost, int servicePort) {
        this(serviceInterfaceClass, serviceHost, servicePort, DEFAULT_TIMEOUT);
    }

    public ReferenceConfig(Class serviceInterfaceClass,
                           String serviceHost,
                           int servicePort,
                           long timeout) {
        this.serviceInterfaceClass = serviceInterfaceClass;
        this.serviceHost = serviceHost;
        this.servicePort = servicePort;
        this.timeout = timeout;
    }

    public Class getServiceInterfaceClass() {
        return serviceInterfaceClass;
    }

    public void setServiceInterfaceClass(Class serviceInterfaceClass) {
        this.serviceInterfaceClass = serviceInterfaceClass;
    }

    public String getServiceHost() {
        return serviceHost;
    }

    public void setServiceHost(String serviceHost) {
        this.serviceHost = serviceHost;
    }

    public int getServicePort() {
        return servicePort;
    }

    public void setServicePort(int servicePort) {
        this.servicePort = servicePort;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    @Override
    public String toString() {
        return "ReferenceConfig{" +
                "serviceInterfaceClass=" + serviceInterfaceClass +
                ", serviceHost='" + serviceHost + '\'' +
                ", servicePort=" + servicePort +
                ", timeout=" + timeout +
                '}';
    }
}
