package com.young.qinger9.netty.rpc.provider;

import com.young.qinger9.netty.rpc.common.RpcRequest;
import com.young.qinger9.netty.rpc.common.RpcResponse;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 9:54
 * @Description: Netty Rpc 服务端处理器
 */
public class NettyRpcServerHandler extends ChannelInboundHandlerAdapter {

    private static final Logger LOGGER = LogManager.getLogger(NettyRpcServerHandler.class);

    private ConcurrentHashMap<String, ServiceConfig> serviceConfigMap =
            new ConcurrentHashMap<>();

    public NettyRpcServerHandler(List<ServiceConfig> serviceConfigs) {
        for (ServiceConfig serviceConfig : serviceConfigs) {
            String serviceInterfaceClass = serviceConfig.getServiceInterfaceClass().getName();
            serviceConfigMap.put(serviceInterfaceClass, serviceConfig);
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        RpcRequest rpcRequest = (RpcRequest) msg;
        LOGGER.info("netty rpc server receive the request: " + rpcRequest);

        RpcResponse rpcResponse = new RpcResponse();
        rpcResponse.setRequestId(rpcRequest.getRequestId());

        try {
            // 通过报文，获取目标class，反射调用拿到返回值
            ServiceConfig serviceConfig = serviceConfigMap.get(rpcRequest.getServiceInterfaceClass());
            Class clazz = serviceConfig.getServiceClass();
            Object instance = clazz.newInstance();
            Method method = clazz.getMethod(rpcRequest.getMethodName(), rpcRequest.getParameterTypes());
            Object result = method.invoke(instance, rpcRequest.getArgs());

            // 封装响应
            rpcResponse.setResult(result);
            rpcResponse.setSuccess(RpcResponse.SUCCESS);
        } catch (Exception e) {
            LOGGER.error("netty rpc server failed to response the request.", e);
            rpcResponse.setResult(RpcResponse.FAILURE);
            rpcResponse.setException(e);
        }

        ctx.write(rpcResponse);
        ctx.flush();
        LOGGER.info("send rpc response to client: " + rpcResponse);
    }
}
