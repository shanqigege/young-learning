package com.young.qinger9.netty.rpc.provider;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 9:55
 * @Description: 服务配置
 */
public class ServiceConfig {

    /**
     * 服务名
     */
    private String serviceName;
    /**
     * 服务接口类
     */
    private Class serviceInterfaceClass;
    /**
     * 服务接口实现类
     */
    private Class serviceClass;


    public ServiceConfig(String serviceName, Class serviceInterfaceClass, Class serviceClass) {
        this.serviceName = serviceName;
        this.serviceInterfaceClass = serviceInterfaceClass;
        this.serviceClass = serviceClass;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Class getServiceInterfaceClass() {
        return serviceInterfaceClass;
    }

    public void setServiceInterfaceClass(Class serviceInterfaceClass) {
        this.serviceInterfaceClass = serviceInterfaceClass;
    }

    public Class getServiceClass() {
        return serviceClass;
    }

    public void setServiceClass(Class serviceClass) {
        this.serviceClass = serviceClass;
    }

    @Override
    public String toString() {
        return "ServiceConfig{" +
                "serviceName='" + serviceName + '\'' +
                ", serviceInterfaceClass=" + serviceInterfaceClass +
                ", serviceClass=" + serviceClass +
                '}';
    }
}
