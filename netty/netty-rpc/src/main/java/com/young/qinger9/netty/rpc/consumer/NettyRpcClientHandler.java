package com.young.qinger9.netty.rpc.consumer;

import com.young.qinger9.netty.rpc.common.RpcResponse;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 11:29
 * @Description: Netty Rpc客户端处理器
 */
public class NettyRpcClientHandler extends ChannelInboundHandlerAdapter {

    private static final Logger LOGGER = LogManager.getLogger(NettyRpcClientHandler.class);

    private static final long GET_RPC_RESPONSE_SLEEP_INTERVAL = 5;

    private ConcurrentHashMap<String, RpcResponse> rpcResponses =
            new ConcurrentHashMap<>();

    /**
     * 超时时间
     */
    private long timeout;

    public NettyRpcClientHandler(long timeout) {
        this.timeout = timeout;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        RpcResponse rpcResponse = (RpcResponse) msg;
        if (rpcResponse.getTimeout()) {
            LOGGER.error("netty rpc client receives the response timeout: " + rpcResponse);
        } else {
            rpcResponses.put(rpcResponse.getRequestId(), rpcResponse);
        }
    }

    public RpcResponse getRpcResponse(String requestId) throws NettyRpcReadTimeoutException {
        long waitStartTime = new Date().getTime();

        while (Objects.isNull(rpcResponses.get(requestId))) {
            try {
                long now = new Date().getTime();
                if (now - waitStartTime >= timeout) {
                    break;
                }
                Thread.sleep(GET_RPC_RESPONSE_SLEEP_INTERVAL);
            } catch (InterruptedException e) {
                LOGGER.error("wait for response interrupted.", e);
            }
        }

        RpcResponse rpcResponse = rpcResponses.get(requestId);

        if (Objects.isNull(rpcResponse)) {
            LOGGER.error("get rpc response timeout.");
            throw new NettyRpcReadTimeoutException("get rpc response timeout");
        } else {
            rpcResponses.remove(requestId);
        }
        return rpcResponse;
    }
}
