package com.young.qinger9.netty.rpc.common;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 9:56
 * @Description: 测试服务接口
 */
public interface TestService {

    String query(String id);
}
