package com.young.qinger9.netty.rpc.common;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 9:57
 * @Description: 测试接口实现类
 */
public class TestServiceImpl implements TestService {
    @Override
    public String query(String id) {
        return "Query result: " + id;
    }
}
