package com.young.qinger9.netty.rpc.common;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 9:22
 * @Description: Rpc 序列化编码
 */
public class RpcEncoder extends MessageToByteEncoder {

    /**
     * 目标类
     */
    private Class<?> targetClass;

    public RpcEncoder(Class<?> targetClass) {
        this.targetClass = targetClass;
    }

    /**
     * 序列化解码
     *
     * @param channelHandlerContext ChannelHandler上下文
     * @param o                     对象
     * @param byteBuf               缓冲区
     * @throws Exception 异常
     */
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object o, ByteBuf byteBuf) throws Exception {
        if (targetClass.isInstance(o)) {
            byte[] bytes = HessianSerialization.serialize(o);
            // 这条报文格式 数据长度(4个字节)+数据
            byteBuf.writeInt(bytes.length);
            byteBuf.writeBytes(bytes);
        }
    }
}
