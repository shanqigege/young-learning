package com.young.qinger9.netty.rpc.common;

import java.io.Serializable;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 9:19
 * @Description: RPC 响应
 */
public class RpcResponse implements Serializable {

    public static final Boolean SUCCESS = true;
    public static final Boolean FAILURE = false;

    /**
     * 请求id
     */
    private String requestId;
    /**
     * 响应成功
     */
    private boolean isSuccess;
    /**
     * 调用结果
     */
    private Object result;
    /**
     * 异常
     */
    private Throwable exception;
    /**
     * 是否超时
     */
    private Boolean timeout = false;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public Throwable getException() {
        return exception;
    }

    public void setException(Throwable exception) {
        this.exception = exception;
    }

    public Boolean getTimeout() {
        return timeout;
    }

    public void setTimeout(Boolean timeout) {
        this.timeout = timeout;
    }

    @Override
    public String toString() {
        return "RpcResponse{" +
                "requestId='" + requestId + '\'' +
                ", isSuccess=" + isSuccess +
                ", result=" + result +
                ", exception=" + exception +
                ", timeout=" + timeout +
                '}';
    }
}
