package com.young.qinger9.netty.rpc.consumer;

import com.young.qinger9.netty.rpc.common.TestService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 11:30
 * @Description: 测试类
 */
public class NettyRpcClientTest {

    private static final Logger LOGGER = LogManager.getLogger(NettyRpcClientTest.class);

    public static void main(String[] args) {
        ReferenceConfig referenceConfig = new ReferenceConfig(TestService.class);
        TestService testService = (TestService) RpcServiceProxy.createProxy(referenceConfig);
        String result = testService.query("id");
        LOGGER.info("rpc remote call result is : " + result);
    }
}

