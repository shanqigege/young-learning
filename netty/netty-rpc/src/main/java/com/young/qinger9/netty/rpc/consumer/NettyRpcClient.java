package com.young.qinger9.netty.rpc.consumer;

import com.young.qinger9.netty.rpc.common.RpcDecoder;
import com.young.qinger9.netty.rpc.common.RpcEncoder;
import com.young.qinger9.netty.rpc.common.RpcRequest;
import com.young.qinger9.netty.rpc.common.RpcResponse;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.internal.StringUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.Objects;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 11:29
 * @Description: Netty Rpc客户端
 */
public class NettyRpcClient {

    private static final Logger LOGGER = LogManager.getLogger(NettyRpcClient.class);

    /**
     * 引用配置
     */
    private ReferenceConfig referenceConfig;
    private ChannelFuture channelFuture;
    private NettyRpcClientHandler nettyRpcClientHandler;

    public NettyRpcClient(ReferenceConfig referenceConfig) {
        this.referenceConfig = referenceConfig;
        this.nettyRpcClientHandler = new NettyRpcClientHandler(referenceConfig.getTimeout());
    }

    public void connect() {
        LOGGER.error("connecting to netty rpc server: " +
                referenceConfig.getServiceHost() + ":" + referenceConfig.getServicePort());

        NioEventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(eventLoopGroup)
                .channel(NioSocketChannel.class)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline()
                                .addLast(new RpcEncoder(RpcRequest.class))
                                .addLast(new RpcDecoder(RpcResponse.class))
                                .addLast(new NettyRpcReadTimeoutHandler(referenceConfig.getTimeout()))
                                .addLast(nettyRpcClientHandler);
                    }
                });

        try {
            if (referenceConfig.getServiceHost() != null
                    && !referenceConfig.getServiceHost().equals("")) {
                channelFuture = bootstrap
                        .connect(referenceConfig.getServiceHost(), referenceConfig.getServicePort())
                        .sync();
                LOGGER.info("successfully connected.");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public RpcResponse remoteCall(RpcRequest rpcRequest) throws Throwable {
        NettyRpcReadTimeoutHolder.put(rpcRequest.getRequestId(), new Date().getTime());

        channelFuture.channel().writeAndFlush(rpcRequest).sync();

        RpcResponse rpcResponse = nettyRpcClientHandler.getRpcResponse(rpcRequest.getRequestId());

        if (rpcResponse.isSuccess()) {
            return rpcResponse;
        }

        throw rpcResponse.getException();
    }
}
