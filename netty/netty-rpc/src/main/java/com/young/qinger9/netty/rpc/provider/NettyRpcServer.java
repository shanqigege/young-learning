package com.young.qinger9.netty.rpc.provider;

import com.young.qinger9.netty.rpc.common.*;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 9:54
 * @Description: Netty Rpc 服务端
 */
public class NettyRpcServer {

    private static final Logger LOGGER = LogManager.getLogger(NettyRpcServer.class);
    /**
     * 默认端口口
     */
    private static final int DEFAULT_PORT = 8998;
    /**
     * 服务配置列表
     */
    private List<ServiceConfig> serviceConfigs = new CopyOnWriteArrayList<>();
    /**
     * 端口号
     */
    private int port;

    public NettyRpcServer(int port) {
        this.port = port;
    }

    public void start() {
        LOGGER.info("netty rpc server starting...");

        NioEventLoopGroup bossEventLoopGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerEventLoopGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossEventLoopGroup, workerEventLoopGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline()
                                    .addLast(new RpcDecoder(RpcRequest.class))
                                    .addLast(new RpcEncoder(RpcResponse.class))
                                    .addLast(new NettyRpcServerHandler(serviceConfigs));
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            // 启动监听指定端口
            ChannelFuture channelFuture = serverBootstrap.bind(port).sync();
            LOGGER.info("netty rpc server started successfully, listened[" + port + "]");
            channelFuture.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            LOGGER.error("netty rpc server failed to start,listened [" + port + "]");
        } finally {
            bossEventLoopGroup.shutdownGracefully();
            workerEventLoopGroup.shutdownGracefully();
        }
    }

    public void addServiceConfig(ServiceConfig serviceConfig) {
        this.serviceConfigs.add(serviceConfig);
    }

    public static void main(String[] args) {
        ServiceConfig serviceConfig = new ServiceConfig("TestService",
                TestService.class,
                TestServiceImpl.class);
        NettyRpcServer nettyRpcServer = new NettyRpcServer(DEFAULT_PORT);
        nettyRpcServer.addServiceConfig(serviceConfig);
        nettyRpcServer.start();
    }
}
