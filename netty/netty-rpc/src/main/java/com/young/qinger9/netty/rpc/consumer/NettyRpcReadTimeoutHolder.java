package com.young.qinger9.netty.rpc.consumer;

import com.sun.org.apache.regexp.internal.RE;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 11:31
 * @Description: 超时Holder
 */
public class NettyRpcReadTimeoutHolder {

    private static ConcurrentHashMap<String, Long> requestTimes =
            new ConcurrentHashMap<>();

    public static void put(String requestId, long timeout) {
        requestTimes.put(requestId, timeout);
    }

    public static long get(String requestId) {
        return requestTimes.get(requestId);
    }

    public static void remove(String requestId) {
        requestTimes.remove(requestId);
    }
}

