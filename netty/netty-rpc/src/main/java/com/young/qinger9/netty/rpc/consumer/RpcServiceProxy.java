package com.young.qinger9.netty.rpc.consumer;

import com.young.qinger9.netty.rpc.common.RpcRequest;
import com.young.qinger9.netty.rpc.common.RpcResponse;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.UUID;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 11:37
 * @Description: Rpc服务动态代理
 */
public class RpcServiceProxy {

    public static Object createProxy(ReferenceConfig referenceConfig) {
        return Proxy.newProxyInstance(
                RpcServiceProxy.class.getClassLoader(),
                new Class[]{referenceConfig.getServiceInterfaceClass()},
                new ServiceProxyInvocationHandler(referenceConfig)
        );
    }

    static class ServiceProxyInvocationHandler implements InvocationHandler {

        private ReferenceConfig referenceConfig;

        public ServiceProxyInvocationHandler(ReferenceConfig referenceConfig) {
            this.referenceConfig = referenceConfig;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            NettyRpcClient nettyRpcClient = new NettyRpcClient(referenceConfig);
            nettyRpcClient.connect();

            RpcRequest rpcRequest = new RpcRequest();
            rpcRequest.setRequestId(UUID.randomUUID().toString().replaceAll("-", ""));
            rpcRequest.setServiceInterfaceClass(referenceConfig.getServiceInterfaceClass().getName());
            rpcRequest.setMethodName(method.getName());
            rpcRequest.setParameterTypes(method.getParameterTypes());
            rpcRequest.setArgs(args);

            RpcResponse rpcResponse = nettyRpcClient.remoteCall(rpcRequest);
            return rpcResponse.getResult();
        }
    }
}
