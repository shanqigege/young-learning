package com.young.qinger9.netty.rpc.consumer;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 11:30
 * @Description: 超时异常
 */
public class NettyRpcReadTimeoutException extends RuntimeException {

    public NettyRpcReadTimeoutException(String message) {
        super(message);
    }
}
