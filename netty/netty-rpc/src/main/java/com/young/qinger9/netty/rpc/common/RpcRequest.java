package com.young.qinger9.netty.rpc.common;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 9:15
 * @Description: Rpc 请求
 */
public class RpcRequest implements Serializable {

    /**
     * 请求id
     */
    private String requestId;
    /**
     * 服务接口类名
     */
    private String serviceInterfaceClass;
    /**
     * 方法名
     */
    private String methodName;
    /**
     * 参数集合类型
     */
    private Class[] parameterTypes;
    /**
     * 方法参数集合
     */
    private Object[] args;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getServiceInterfaceClass() {
        return serviceInterfaceClass;
    }

    public void setServiceInterfaceClass(String serviceInterfaceClass) {
        this.serviceInterfaceClass = serviceInterfaceClass;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Class[] getParameterTypes() {
        return parameterTypes;
    }

    public void setParameterTypes(Class[] parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    @Override
    public String toString() {
        return "RpcRequest{" +
                "requestId='" + requestId + '\'' +
                ", serviceInterfaceClass='" + serviceInterfaceClass + '\'' +
                ", methodName='" + methodName + '\'' +
                ", parameterTypes=" + Arrays.toString(parameterTypes) +
                ", args=" + Arrays.toString(args) +
                '}';
    }
}
