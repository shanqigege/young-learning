package com.young.qinger9.netty.rpc.common;

import com.caucho.hessian.io.HessianInput;
import com.caucho.hessian.io.HessianOutput;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 9:26
 * @Description: Hessian 序列化
 */
public class HessianSerialization {

    /**
     * 序列化
     *
     * @param obj 对象
     * @return 字节流数组
     * @throws IOException IO异常
     */
    public static byte[] serialize(Object obj) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        HessianOutput hessianOutput = new HessianOutput(byteArrayOutputStream);
        hessianOutput.writeObject(obj);
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * 反序列化
     *
     * @param bytes 字节流
     * @param clazz 类
     * @return 对象
     * @throws IOException IO异常
     */
    public static Object deserialize(byte[] bytes, Class clazz) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        HessianInput hessianInput = new HessianInput(byteArrayInputStream);
        return hessianInput.readObject(clazz);
    }
}
