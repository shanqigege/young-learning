package com.young.qinger9.netty.rpc.common;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.sql.ClientInfoStatus;
import java.util.List;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-28 9:32
 * @Description: Rpc 反序列化解码
 */
public class RpcDecoder extends ByteToMessageDecoder {

    /**
     * 报文数据长度占字节数
     */
    private static final int MESSAGE_LENGTH_BYTE = 4;
    /**
     * 报文长度0 -> 非法
     */
    private static final int MESSAGE_LENGTH_VALID_MINIMUM_VALUE = 0;

    /**
     * 目标类
     */
    private Class<?> targetClass;

    public RpcDecoder(Class<?> targetClass) {
        this.targetClass = targetClass;
    }

    /**
     * 反序列化解码
     *
     * @param ctx ChannelHandler上下文
     * @param in  字节缓冲区
     * @param out 反序列化对象
     * @throws Exception 异常
     */
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        // 校验消息长度的字节数，必须达到4个字节
        if (in.readableBytes() < MESSAGE_LENGTH_BYTE) {
            return;
        }

        // 对于ByteBuf当前可以读的readerIndex，可以mark标记
        // 后续可以通过mark标记，找回发起read读取之前的readerIndex位置
        in.markReaderIndex();

        // 读取四个字节的int，代表消息byte的长度
        int messageLength = in.readInt();
        // 如果消息长度小于0，说明报文丢失，通信故障
        if (messageLength < MESSAGE_LENGTH_VALID_MINIMUM_VALUE) {
            // 关闭通道
            ctx.close();
        }

        // 此时消息字节数据可能没有接收完整，可以读取的字节数是比消息字节长度小的
        // 检查经典的拆包问题
        if (in.readableBytes() < messageLength) {
            in.resetReaderIndex();
            return;
        }

        // 反序列化
        byte[] bytes = new byte[messageLength];
        in.readBytes(bytes);
        Object object = HessianSerialization.deserialize(bytes, targetClass);
        out.add(object);
    }
}
