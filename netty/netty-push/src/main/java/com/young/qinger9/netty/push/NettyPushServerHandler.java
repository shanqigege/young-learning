package com.young.qinger9.netty.push;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.util.CharsetUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @Author: youngqinger9
 * @Date: 2022-12-01 9:56
 * @Description: Netty push 服务端处理器
 */
public class NettyPushServerHandler extends SimpleChannelInboundHandler<Object> {

    private static final Logger LOGGER = LogManager.getLogger(NettyPushServerHandler.class);

    private WebSocketServerHandshaker webSocketServerHandshaker;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        LOGGER.info("client connection established: " + ctx.channel());
        ChannelManager.add(ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        LOGGER.info("client disconnected: " + ctx.channel());
        ChannelManager.remove(ctx.channel());
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof FullHttpRequest) {
            handleHttpRequest(ctx, (FullHttpRequest) msg);
        } else {
            handleWebSocketFrame(ctx, (WebSocketFrame) msg);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    /**
     * 处理http协议请求
     *
     * @param ctx 上下文
     * @param msg 请求
     */
    private void handleHttpRequest(ChannelHandlerContext ctx, FullHttpRequest msg) {
        if (!msg.decoderResult().isSuccess() ||
                (!"websocket".equals(msg.headers().get("Upgrade")))) {
            DefaultFullHttpResponse response = new DefaultFullHttpResponse(
                    HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST
            );
            sendHttpResponse(ctx, msg, response);
            return;
        }

        LOGGER.info("receives handshake request from client: " + ctx.channel());

        WebSocketServerHandshakerFactory webSocketServerHandshakerFactory =
                new WebSocketServerHandshakerFactory("ws://localhost:8998/push",
                        null,
                        false);
        webSocketServerHandshaker = webSocketServerHandshakerFactory.newHandshaker(msg);
        if (webSocketServerHandshaker == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            webSocketServerHandshaker.handshake(ctx.channel(), msg);
            LOGGER.info("netty push server handshake with client: " + ctx.channel());
        }
    }

    private void sendHttpResponse(ChannelHandlerContext ctx, FullHttpRequest msg, DefaultFullHttpResponse response) {
        if (response.status().code() != HttpResponseStatus.OK.code()) {
            ByteBuf byteBuf = Unpooled.copiedBuffer(
                    response.status().toString(),
                    CharsetUtil.UTF_8);
            LOGGER.info("http response is not ok: " + byteBuf.toString(CharsetUtil.UTF_8));
            byteBuf.release();
        }

        ChannelFuture channelFuture = ctx.channel().writeAndFlush(response);

        if (response.status().code() != HttpResponseStatus.OK.code()) {
            channelFuture.addListener(ChannelFutureListener.CLOSE);
        }
    }

    /**
     * 处理器websocket协议请求
     *
     * @param ctx 协议
     * @param msg 请求
     */
    private void handleWebSocketFrame(ChannelHandlerContext ctx, WebSocketFrame msg) {
        // websocket client一直发送ping，看长连接是否存活和有效
        if (msg instanceof PingWebSocketFrame) {
            LOGGER.info("receives ping frame from client: " + ctx.channel());
            PongWebSocketFrame pongWebSocketFrame = new PongWebSocketFrame(
                    msg.content().retain());
            ctx.channel().write(pongWebSocketFrame);
            return;
        }

        // websocket client 发送请求，不是text文本请求
        if (!(msg instanceof TextWebSocketFrame)) {
            LOGGER.info("netty push server only support text frame,does not support other type frame.");
            String errorMsg = String.format("%s type frame is not support",
                    msg.getClass().getName());
            throw new UnsupportedOperationException(errorMsg);
        }

        // websocket client发送关闭连接请求
        if (msg instanceof CloseWebSocketFrame) {
            LOGGER.info("receives close websocket request from client: " + ctx.channel());
            webSocketServerHandshaker.close(ctx.channel(),
                    ((CloseWebSocketFrame) msg).retain());
            return;
        }

        // socket client 发送text frame类型请求
        String request = ((TextWebSocketFrame) msg).text();
        LOGGER.info("receives text frame[" + request + "] from client" + ctx.channel());

        TextWebSocketFrame response = new TextWebSocketFrame(request);
        ChannelManager.pushToAllChannels(response);
    }
}

