package com.young.qinger9.netty.sticky;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-29 19:35
 * @Description: Netty客户端
 */
public class NettyStickyPacketClient {

    private static final Logger LOGGER = LogManager.getLogger(NettyStickyPacketClient.class);
    private static final String DELIMITER = "$%%$";
    private static final int RESPONSE_BYTES_LENGTH = 42;

    private static final String DEFAULT_HOST = "127.0.0.1";
    private static final int DEFAULT_PORT = 8998;

    private final String host;
    private final int port;

    public NettyStickyPacketClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void connect() throws Exception {
        LOGGER.info("Netty client connected...");

        NioEventLoopGroup eventLoopGroup = new NioEventLoopGroup();

        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(eventLoopGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline()
//                                    .addLast(new LineBasedFrameDecoder(1024))
//                                    .addLast(new DelimiterBasedFrameDecoder(
//                                            1024,
//                                            Unpooled.copiedBuffer(DELIMITER.getBytes())
//                                    ))
                                    .addLast(new FixedLengthFrameDecoder(RESPONSE_BYTES_LENGTH))
                                    .addLast(new StringDecoder())
                                    .addLast(new NettyStickyPacketClientHandler());
                        }
                    });

            ChannelFuture channelFuture = bootstrap.connect(host, port).sync();
            LOGGER.info("server connected: " + host + ":" + port + ".");
            channelFuture.channel().closeFuture().sync();
        } finally {
            eventLoopGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception {
        NettyStickyPacketClient nettyStickyPacketClient =
                new NettyStickyPacketClient(DEFAULT_HOST, DEFAULT_PORT);
        nettyStickyPacketClient.connect();
    }
}
