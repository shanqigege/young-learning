package com.young.qinger9.netty.sticky;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-29 14:24
 * @Description: 服务端
 */
public class NettyStickyPacketServer {

    private static final Logger LOGGER = LogManager.getLogger(NettyStickyPacketServer.class);

    private static final String DELIMITER = "$%%$";

    private static final int PING_BYTES_LENGTH = 17;

    private static final int DEFAULT_PORT = 8998;

    private final int port;

    public NettyStickyPacketServer(int port) {
        this.port = port;
    }

    public void start() throws Exception {
        LOGGER.info("Netty server starting...");

        NioEventLoopGroup bossEventLoopGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerEventLoopGroup = new NioEventLoopGroup();

        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossEventLoopGroup, workerEventLoopGroup)
                    .channel(NioServerSocketChannel.class)
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline()
                                    // 读取一行\n \r\n，最大1024个字节
//                                    .addLast(new LineBasedFrameDecoder(1024))
                                    // 分隔符读取，最大1024个字节
//                                    .addLast(new DelimiterBasedFrameDecoder(
//                                            1024,
//                                            Unpooled.copiedBuffer(DELIMITER.getBytes())
//                                    ))
                                    .addLast(new FixedLengthFrameDecoder(PING_BYTES_LENGTH))
                                    .addLast(new StringDecoder())
                                    .addLast(new NettyStickyPacketServerHandler());
                        }
                    });
            ChannelFuture channelFuture = serverBootstrap.bind(port).sync();
            LOGGER.info("netty server started,listened [" + port + "]");
            channelFuture.channel().closeFuture().sync();
        } finally {
            bossEventLoopGroup.shutdownGracefully();
            workerEventLoopGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception {
        NettyStickyPacketServer nettyStickyPacketServer = new NettyStickyPacketServer(DEFAULT_PORT);
        nettyStickyPacketServer.start();
    }
}
