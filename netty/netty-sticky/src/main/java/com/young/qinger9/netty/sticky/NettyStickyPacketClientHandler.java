package com.young.qinger9.netty.sticky;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-29 11:53
 * @Description: 客户端处理器
 */
public class NettyStickyPacketClientHandler extends ChannelInboundHandlerAdapter {

    private static final Logger LOGGER = LogManager.getLogger(NettyStickyPacketClientHandler.class);

    private final byte[] pingBytes;

    public NettyStickyPacketClientHandler() {
        this.pingBytes = "Netty client call".getBytes();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ByteBuf byteBuf;
        for (int i = 0; i < 120; i++) {
            byteBuf = Unpooled.buffer(pingBytes.length);
            byteBuf.writeBytes(pingBytes);
            ctx.writeAndFlush(byteBuf);
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        String response = (String) msg;
        LOGGER.info("netty client received response: " + response);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}
