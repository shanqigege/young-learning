package com.young.qinger9.netty.http;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-29 20:05
 * @Description: 服务端处理器
 */
public class NettyHttpServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    private static final Logger LOGGER = LogManager.getLogger(NettyHttpServerHandler.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) throws Exception {
        String method = msg.method().name();
        String uri = msg.uri();
        LOGGER.info("receives http request: " + method + " " + uri + ".");

        String html = "<html><body>Netty http server call back.</body></html>";
        ByteBuf byteBuf = Unpooled.copiedBuffer(html, CharsetUtil.UTF_8);

        DefaultFullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
        response.headers().set("content-type", "text/html;charset=UTF-8");
        response.content().writeBytes(byteBuf);

        // 释放缓冲区
        byteBuf.release();

        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }
}
