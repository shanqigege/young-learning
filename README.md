# young-learning

#### 一、介绍
    技术栈学习过程记录，API应用，场景驱动开发简单组件。
#### 二、内容
#### 1.ElasticSearch
* Examples from the official website
* ****...****
#### 2.Netty
* Module netty-sample
  * Simple Netty Client & Server
* Module netty-rpc 
  * Simple Netty Rpc
* Module netty-sticky 
  * Network sticking and unpacking
* Module netty-http
  * HTTP server
* Module netty-websocket
  * Websocket server
* Module netty-push
  * Push system
* ****...****
    