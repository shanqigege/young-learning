package com.young.qinger9.elasticsearch.sample;

import co.elastic.clients.elasticsearch.ElasticsearchAsyncClient;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.IndexRequest;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import com.young.qinger9.elasticsearch.entity.Product;
import com.young.qinger9.elasticsearch.tools.ElasticClientFactory;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-15 19:40
 * @Description: 索引单条文档操作
 */
public class IndexingSingleDoc {

    private static final Product product = new Product("bk-1", "City bike", 123.0);

    private static final ElasticsearchClient client = ElasticClientFactory.initSyncClient();
    private static final ElasticsearchAsyncClient asyncClient = ElasticClientFactory.initAsyncClient();

    public static void main(String[] args) throws IOException {
        usingTheFluentDSL();
        usingClassicBuilders();
        usingAsyncClient();
        usingRawJSONData();

        System.exit(0);
    }

    private static void usingRawJSONData() throws IOException {
        Reader input = new StringReader(
                "{'@timestamp': '2022-04-08T13:55:32Z', 'level': 'warn', 'message': 'Some log message'}"
                        .replace('\'', '"'));
        IndexRequest<Object> request = IndexRequest.of(idx -> idx
                .index("logs")
                .withJson(input));
        assert client != null;
        IndexResponse resp = client.index(request);
        System.out.println("Indexed with version:" + resp.version());
    }

    private static void usingAsyncClient(){
        assert asyncClient != null;
        asyncClient.index(idx -> idx
                .index("products")
                .id(product.getSku())
                .document(product))
                .whenComplete(((indexResponse, throwable) -> {
                    if(throwable != null){
                        System.out.println("Failed to index：" + throwable);
                    }else{
                        System.out.println("Indexed with version:" + indexResponse.version());
                    }
                }));
    }

    private static void usingClassicBuilders() throws IOException {
        IndexRequest.Builder<Product> builder = new IndexRequest.Builder<>();
        builder.index("products");
        builder.id(product.getSku());
        builder.document(product);

        assert client != null;
        IndexResponse resp = client.index(builder.build());
        System.out.println("Indexed with version:" + resp.version());
    }

    private static void usingTheFluentDSL() throws IOException {
        assert client != null;
        // fluent
        IndexResponse resp0 = client.index(idx -> idx
                .index("products")
                .id(product.getSku())
                .document(product));
        System.out.println("Indexed with version:" + resp0.version());

        // static
        IndexRequest<Object> request = IndexRequest.of(idx -> idx
                .index("products")
                .id(product.getSku())
                .document(product));
        IndexResponse resp1 = client.index(request);
        System.out.println("Indexed with version:" + resp1.version());
    }
}
