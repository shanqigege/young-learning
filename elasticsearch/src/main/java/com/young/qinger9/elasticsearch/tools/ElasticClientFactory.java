package com.young.qinger9.elasticsearch.tools;

import co.elastic.clients.elasticsearch.ElasticsearchAsyncClient;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.TransportUtils;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-15 16:04
 * @Description: es client factory
 */
public class ElasticClientFactory {

    private static final String CRT =  System.getProperty("user.dir") + "\\elasticsearch" +  "\\src\\main\\resources\\http_ca.crt";

    private ElasticClientFactory() {
    }

    public static ElasticsearchClient initSyncClient() {
        try {
            RestClientTransport transport = getRestClientTransport();
            return new ElasticsearchClient(transport);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ElasticsearchAsyncClient initAsyncClient() {
        try {
            RestClientTransport transport = getRestClientTransport();
            return new ElasticsearchAsyncClient(transport);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static RestClientTransport getRestClientTransport() throws IOException {
        File file = new File(CRT);
        SSLContext sslContext = TransportUtils
                .sslContextFromHttpCaCrt(file);

        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("elastic", "hadoop"));

        RestClient restClient = RestClient.builder(
                        new HttpHost("127.0.0.1", 19200, "https"))
                .setHttpClientConfigCallback(hc -> hc
                        .setSSLContext(sslContext)
                        .setDefaultCredentialsProvider(credentialsProvider))
                .build();
        return new RestClientTransport(
                restClient, new JacksonJsonpMapper());
    }
}
