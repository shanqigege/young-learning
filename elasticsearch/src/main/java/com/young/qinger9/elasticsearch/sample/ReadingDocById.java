package com.young.qinger9.elasticsearch.sample;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.GetResponse;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.young.qinger9.elasticsearch.entity.Product;
import com.young.qinger9.elasticsearch.tools.ElasticClientFactory;

import java.io.IOException;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-16 12:00
 * @Description: 通过id读doc
 */
public class ReadingDocById {

    static final ElasticsearchClient client = ElasticClientFactory.initSyncClient();

    public static void main(String[] args) throws IOException {
//        readingDomainObj();
        readingRawJSON();
    }

    private static void readingRawJSON() throws IOException {
        GetResponse<ObjectNode> resp = client.get(g ->
                        g.index("products")
                                .id("bk-1"),
                ObjectNode.class);
        if (resp.found()) {
            ObjectNode json = resp.source();
            String name = json.get("name").asText();
            System.out.println("Get product:" + name);
        } else {
            System.out.println("Product not found");
        }
    }

    private static void readingDomainObj() throws IOException {
        assert client != null;
        GetResponse<Product> resp = client.get(fn -> fn
                        .index("products")
                        .id("bk-1"),
                Product.class);
        if (resp.found()) {
            Product source = resp.source();
            System.out.println("Get product:" + source);
        } else {
            System.out.println("Product not found");
        }
    }
}
