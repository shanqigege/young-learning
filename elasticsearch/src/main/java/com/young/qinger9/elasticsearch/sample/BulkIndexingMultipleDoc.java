package com.young.qinger9.elasticsearch.sample;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import co.elastic.clients.json.JsonData;
import co.elastic.clients.json.JsonpMapper;
import com.young.qinger9.elasticsearch.entity.Product;
import com.young.qinger9.elasticsearch.tools.ElasticClientFactory;
import jakarta.json.spi.JsonProvider;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-15 20:22
 * @Description: 批量文档
 */
public class BulkIndexingMultipleDoc {

    static List<Product> products;

    static ElasticsearchClient client = ElasticClientFactory.initSyncClient();

    static {
        Product p0 = new Product("bk-1", "City bike", 523.0);
        Product p1 = new Product("bk-2", "Country bike", 1123.0);
        Product p2 = new Product("bk-3", "Side bike", 12223.0);

        products = new ArrayList<>();
        products.add(p0);
        products.add(p1);
        products.add(p2);

    }

    public static void main(String[] args) throws IOException {
        // DSL Bulk request
        //indexingAppObj();

        // bulk raw json
        indexingRawJSONData();
    }

    private static JsonData readJson(InputStream inputStream, ElasticsearchClient client) {
        JsonpMapper jsonpMapper = client._transport().jsonpMapper();
        JsonProvider jsonProvider = jsonpMapper.jsonProvider();
        return JsonData.from(jsonProvider.createParser(inputStream), jsonpMapper);
    }

    private static void indexingRawJSONData() throws IOException {
        File logDir = new File("E:\\CodingDirectory\\yzqing\\young-tools\\elasticsearch\\src\\main\\resources\\logs");
        File[] files = logDir.listFiles(file -> file
                .getName().matches("log-.*\\.json"));
        BulkRequest.Builder br = new BulkRequest.Builder();

        assert files != null;
        for (File file : files) {
            JsonData jsonData = readJson(new FileInputStream(file), client);

            br.operations(op -> op
                    .index(idx -> idx
                            .index("logs")
                            .document(jsonData)
                    )
            );
        }

        BulkResponse resp = client.bulk(br.build());
    }

    private static void indexingAppObj() throws IOException {
        BulkRequest.Builder br = new BulkRequest.Builder();
        for (Product product : products) {
            br.operations(op -> op
                    .index(idx -> idx
                            .index("products")
                            .id(product.getSku())
                            .document(product)));
        }

        BulkResponse resp = client.bulk(br.build());

        if (resp.errors()) {
            List<BulkResponseItem> items = resp.items();
            for (BulkResponseItem item : items) {
                if (item.error() != null) {
                    System.out.println(item.error().reason());
                }
            }
        }
    }
}
