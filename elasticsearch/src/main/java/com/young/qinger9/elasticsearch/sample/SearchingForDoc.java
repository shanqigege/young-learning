package com.young.qinger9.elasticsearch.sample;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.RangeQuery;
import co.elastic.clients.elasticsearch.core.PutScriptResponse;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.SearchTemplateResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.TotalHits;
import co.elastic.clients.elasticsearch.core.search.TotalHitsRelation;
import co.elastic.clients.json.JsonData;
import com.sun.scenario.effect.impl.prism.PrImage;
import com.young.qinger9.elasticsearch.entity.Product;
import com.young.qinger9.elasticsearch.tools.ElasticClientFactory;

import java.io.IOException;
import java.util.List;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-17 11:06
 * @Description: 查询doc.
 */
public class SearchingForDoc {

    static final ElasticsearchClient client = ElasticClientFactory.initSyncClient();

    public static void main(String[] args) throws IOException {
//        simpleSearchQuery();
//        nestedSearchQueries();
        templateSearch();
    }

    private static void templateSearch() throws IOException {
        assert client != null;
        // Identifier of the template script to create.
        PutScriptResponse respScript = client.putScript(r -> r
                .id("product-script-template")
                .script(s -> s
                        .lang("mustache")
                        .source("\"query\":{\n" +
                                "        \"match\":{\n" +
                                "          \"sku\": \"{{sku}}\"\n" +
                                "        }")));
        // use template
        SearchTemplateResponse<Product> resp = client.searchTemplate(r -> r
                .index("products")
                .id("product-script-template")
                .params("sku", JsonData.of("bk-1"))
                , Product.class);

        List<Hit<Product>> hits = resp.hits().hits();
        for (Hit<Product> hit : hits) {
            System.out.println(hit.source());
        }
    }

    private static void nestedSearchQueries() throws IOException {
        String searchTxt = "bike";
        double maxPrice = 1000.0;

        Query byName = MatchQuery.of(m -> m
                        .field("name")
                        .query(searchTxt))
                ._toQuery();

        Query byMaxPrice = RangeQuery.of(r -> r
                        .field("price")
                        .gte(JsonData.of(maxPrice)))
                ._toQuery();

        assert client != null;
        SearchResponse<Product> resp = client.search(s -> s
                        .index("products")
                        .query(q -> q
                                .bool(b -> b
                                        .must(byName)
                                        .must(byMaxPrice))),
                Product.class);

        List<Hit<Product>> hits = resp.hits().hits();
        for (Hit<Product> hit : hits) {
            System.out.println(hit.source());
        }
    }

    private static void simpleSearchQuery() throws IOException {
        String searchTxt = "bike";

        assert client != null;
        SearchResponse<Product> resp = client.search(req -> req
                        .index("products")
                        .query(q -> q
                                .match(t -> t
                                        .field("name")
                                        .query(searchTxt))),
                Product.class);

        TotalHits total = resp.hits().total();
        boolean isExactResult = total.relation() == TotalHitsRelation.Eq;

        if (isExactResult) {
            System.out.println("There are " + total.value() + " results");
        } else {
            System.out.println("There are more than " + total.value() + " results");
        }

        List<Hit<Product>> hits = resp.hits().hits();
        for (Hit<Product> hit : hits) {
            Product product = hit.source();
            System.out.println(product);
        }
    }
}
