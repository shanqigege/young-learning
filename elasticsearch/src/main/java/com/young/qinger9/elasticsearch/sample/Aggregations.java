package com.young.qinger9.elasticsearch.sample;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.aggregations.HistogramBucket;
import co.elastic.clients.elasticsearch._types.query_dsl.MatchQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import com.young.qinger9.elasticsearch.tools.ElasticClientFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Author: youngqinger9
 * @Date: 2022-11-17 19:54
 * @Description: 聚合操作
 */
public class Aggregations {

    static ElasticsearchClient client = ElasticClientFactory.initSyncClient();

    public static void main(String[] args) throws IOException {
        simpleAggregation();
    }

    private static void simpleAggregation() throws IOException {
        String searchText = "bike";

        Query query = MatchQuery.of(m -> m
                .field("name")
                .query(searchText)
        )._toQuery();

        SearchResponse<Void> resp = client.search(b -> b
                        .index("products")
                        .size(0)
                        .query(query)
                        .aggregations("price-histogram", a -> a
                                .histogram(h -> h
                                        .field("price")
                                        .interval(50.0)
                                )
                        ),
                Void.class
        );

        List<HistogramBucket> buckets = resp.aggregations()
                .get("price-histogram")
                .histogram()
                .buckets().array();

        for (HistogramBucket bucket : buckets) {
            System.out.println("There are " + bucket.docCount() +
                    " bikes under " + bucket.key());
        }
    }
}
